import java.awt.Color;

import javax.swing.JFrame;

import walker.AdvancedWalker;
import walker.SimpleWalker;
import walker.Walker;

/**
 * Assignment for Lab 9:
 * Friday 9-Mar-2018
 * 
 * This project contains both what we practiced and what
 * is to be completed as your homework.
 * 
 * 
 * @author Azim Ahmadzadeh - https://grid.cs.gsu.edu/~aahmadzadeh1/
 *
 */

/**
 * This project is implemented to let the students to practice more on the
 * topics of inheritance, encapsulation, abstraction and interfaces.
 */
public class Main {

	public static void main(String arg[]) {

		JFrame frame = new JFrame("LAB 9 Jiggy Material");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		//frame.setBackground(Color.YELLOW);
		//frame.setBackground(Color.PINK);
		

		
		//  TASK 1: Create a SimpleWalker and pass it to Ground's constructor.
		 
		Walker w1 = new SimpleWalker();
		// Ground panel = new Ground(w1);

		// TODO

		/*
		 * TASK 2: Create an AdvancedWalker, set colors to it, change its stepSize, and
		 * also make it larger than the SimpleWalker. Then pass it to the Ground's
		 * constructor.
		 */

		Walker w2 = new AdvancedWalker();
		
		// Because i gave it a value in the class i Do NOT need these
		 ((AdvancedWalker) w2).setShadowToWalker(Color.BLUE);
		 ((AdvancedWalker) w2).setColorToWalker(Color.GREEN);
		((AdvancedWalker) w2).setStepSize(30);
		((AdvancedWalker) w2).enlarge(20);

		Ground panel = new Ground(w1);

		frame.setContentPane(panel);
		frame.setVisible(true);
		panel.setFocusable(true);
		frame.setBackground(Color.PINK);
	}
}
