package walker;

import java.awt.Color;

public class AdvancedWalker extends Walker implements ICustomizable {
	// fields
	private Color walkerColor;
	private Color walkerShadow;

	// Constructor
	public AdvancedWalker() {
		super();
		this.walkerColor = Color.BLACK;
		this.walkerShadow = Color.GRAY;
	}

	@Override
	public void moveRight() {

		this.walkerX = this.walkerX + this.stepSize;
		if(this.walkerX > 800) {
			this.walkerX = 1;
		}

	}

	@Override
	public void moveLeft() {

		this.walkerX = this.walkerX - this.stepSize;
		if(this.walkerX < 0) {
			this.walkerX = 799;
		}

	}

	@Override
	public void moveUp() {

		this.walkerY = this.walkerY - this.stepSize;
		if(this.walkerY < 0) {
			this.walkerY = 599;
		}
	}

	@Override
	public void moveDown() {

		this.walkerY = this.walkerY + this.stepSize;
		if(this.walkerY > 600) {
			this.walkerY = 1;
		}

	}

	@Override
	public void setColorToWalker(Color greg) {
		// TODO Auto-generated method stub
		this.walkerColor = greg;

	}

	@Override
	public void setShadowToWalker(Color sergey) {
		// TODO Auto-generated method stub
		this.walkerShadow = sergey;

	}

	@Override
	public void setStepSize(int size) {
		 this.stepSize = size;

	}

	@Override
	public void enlarge(int factor) {
		// TODO Auto-generated method stub
		this.wHeight = factor;
		this.wWidth = factor;
	}

	// getter
	public Color getWalkerColor() {
		return walkerColor;
	}

	// getters
	public Color getWalkerShadow() {
		return walkerShadow;
	}

}
