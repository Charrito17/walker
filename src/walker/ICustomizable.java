package walker;

import java.awt.Color;

public interface ICustomizable {

	public void setColorToWalker(Color greg);

	public void setShadowToWalker(Color sergey);

	public void setStepSize(int size);

	public void enlarge(int factor);

}
