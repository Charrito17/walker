package walker;

import java.awt.Color;

public class SimpleWalker extends Walker {
	// fields
	private Color walkerColor;
	private Color walkerShadow;

	// Constructor
	public SimpleWalker() {
		super();
		this.walkerColor = Color.BLACK;
		this.walkerShadow = Color.GRAY;
	}

	@Override
	public void moveRight() {

		this.walkerX = this.walkerX + this.stepSize;
		if (this.walkerX > 800) {
			this.walkerX = 1;
		}

	}

	@Override
	public void moveLeft() {

		this.walkerX = this.walkerX - this.stepSize;
		if(this.walkerX < 0) {
			this.walkerX = 799;
		}

	}

	@Override
	public void moveUp() {

		this.walkerY = this.walkerY - this.stepSize;
		if(this.walkerY < 0) {
			this.walkerY = 599;
		}

	}

	@Override
	public void moveDown() {

		this.walkerY = this.walkerY + this.stepSize;
		if(this.walkerY > 600) {
			this.walkerY = 1;
		}

	}

	// getters
	public Color getWalkerColor() {
		return walkerColor;
	}

	// getters
	public Color getWalkerShadow() {
		return walkerShadow;
	}

}
