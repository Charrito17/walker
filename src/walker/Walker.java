package walker;

public abstract class Walker {

	protected int initial_x = 1;
	protected int initial_y = 1;
	protected int walkerX = initial_x;
	protected int walkerY = initial_y;

	protected int wHeight = 5;
	protected int wWidth = 5;
	protected int stepSize = 8;

	public abstract void moveRight();

	public abstract void moveLeft();

	public abstract void moveUp();

	public abstract void moveDown();

	public int getInitial_X() {
		return initial_x;
	}

	// Setters have parameters
	public void setWalker_x(int initialX) {
		this.initial_x = initialX;

	}

	public int getInitial_Y() {
		return initial_y;
	}

	// Setters have parameters
	public void setInitial_Y(int initialY) {
		this.initial_y = initialY;
	}

	public int getWalker_x() {
		return walkerX;
	}

	// Setters have parameters
	public void setWalkerX(int walkerX) {
		this.walkerX = walkerX;
	}

	public int getWalker_y() {
		return walkerY;
	}

	// Setters have parameters
	public void setWalker_y(int walkerY) {
		this.walkerY = walkerY;
	}

	public int getwHeight() {
		return wHeight;
	}

	// Setters have parameters
	public void setwHeight(int wHeight) {
		this.wHeight = wHeight;

	}

	public int getwWidth() {
		return wWidth;
	}

	// Setters have parameters
	public void setwWidth(int wWidth) {
		this.wWidth = wWidth;
	}

}
